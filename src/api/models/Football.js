const sequelize = require('sequelize');
const db = require('../../config/sequelize');

const Football = db.define('football', {
    name: {
        type: sequelize.STRING,
        allowNull: false,
    },
    number_of_people: {
        type: sequelize.INTEGER,
        allowNull: false
    },
    unit_charge: {
        type: sequelize.DECIMAL(10, 2),
        allowNull: false,
    },
    amount: {
        type: sequelize.DECIMAL(10, 2),
        allowNull: false
    }
}, {
    underscored: true,
});

Football.sync();

module.exports = Football;

