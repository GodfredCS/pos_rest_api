const sequelize = require('sequelize');
const db = require('../../config/sequelize');
/* const Item = require('./Item'); */

const Role = db.define('role', {
    name: {
        type: sequelize.STRING,
        allowNull: false,
    },
}, {
    underscored: true
});

Role.sync()
    .then(() => {
        Role.bulkCreate([
            { name: 'super_admin'},
            { name: 'admin' }
        ])
    });

module.exports = Role;
