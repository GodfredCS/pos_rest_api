const sequelize = require('sequelize');
const db = require('../../config/sequelize');

const MobileMoney = db.define('mobile_money', {
    name: {
        type: sequelize.STRING,
        allowNull: false
    },
    type: {
        type: sequelize.STRING,
        allowNull: false
    },
    phone: {
        type: sequelize.STRING,
        allowNull: false
    },
    commission: {
        type: sequelize.DECIMAL(10, 2),
        allowNull: false
    },
    amount: {
        type: sequelize.DECIMAL(10, 2),
        allowNull: false
    }
}, {
    underscored: true
});

MobileMoney.sync();

module.exports = MobileMoney;

