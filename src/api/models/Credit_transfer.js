const sequelize = require('sequelize');
const db = require('../../config/sequelize');

const CreditTransfer = db.define('credit_transfer', {
    number: {
        type: sequelize.STRING,
        allowNull: false
    },
    amount: {
        type: sequelize.DECIMAL(10, 2),
        allowNull: false
    }
}, {
    underscored: true,
});

CreditTransfer.sync();

module.exports = CreditTransfer;

