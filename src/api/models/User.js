const sequelize = require('sequelize');
const bcrypt = require('bcrypt');
const db = require('../../config/sequelize');
const { admin_email, admin_password } = require('../../config/variables');

const Role = require('./Role');

const User = db.define('user', {
    role_id: {
        type: sequelize.INTEGER,
        allowNull: false,
        references: {
            model: Role,
            key: 'id',
        },
    },
    firstname: {
        type: sequelize.STRING,
        allowNull: false,
    },
    lastname: {
        type: sequelize.STRING,
        allowNull: true,
    },
    email: {
        type: sequelize.STRING,
        unique: true,
        allowNull: false,
        validate: {
            isEmail: {
                msg: 'Please enter a correct email'
            }
        }
    },
    password: {
        type: sequelize.STRING,
        allowNull: false,
        validate: {
            notEmpty: {
                msg: 'Please provide a password'
            },
            min: 6
        }
    },
    profile_image: {
        type: sequelize.STRING,
        allowNull: true,
    }
}, {
    underscored: true,
});  

User.sync()
    .then(() => {
        User.findOne({ where: { email: admin_email } })
            .then(user => {
                if (!user) {
                    bcrypt.hash(admin_password, 10, (err, hash) => {
                        if (err) {
                            return res.status(500).json(err);
                        }

                        if (hash) {
                            return User.create({
                                role_id: 1,
                                firstname: 'Godfred',
                                lastname: 'Addai',
                                email: admin_email, 
                                password: hash,
                            });
                        }
                    });
                }
            });
    });

module.exports = User;

