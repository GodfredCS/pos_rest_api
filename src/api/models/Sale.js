const sequelize = require('sequelize');
const db = require('../../config/sequelize');
const Item = require('./Item');

const Sales = db.define('sales', {
    item_id: {
        type: sequelize.INTEGER,
        allowNull: false,
        references: {
            model: Item,
            key: 'id',
        },
    },
    unit_quantity: {
        type: sequelize.INTEGER,
        allowNull: false,
    },
    whole_quantity: {
        type: sequelize.INTEGER,
        allowNull: false,
    },
    amount: {
        type: sequelize.DECIMAL(10, 2),
        allowNull: false
    }
}, {
    underscored: true
});

Sales.sync();

module.exports = Sales;
