const sequelize = require('sequelize');
const db = require('../../config/sequelize');

const Jackpot = db.define('jackpot', {
    name: {
        type: sequelize.STRING,
        allowNull: false
    },
    amount: {
        type: sequelize.DECIMAL(10, 2),
        allowNull: false
    }
}, {
    underscored: true,
});

Jackpot.sync();

module.exports = Jackpot;

