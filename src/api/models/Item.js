const sequelize = require('sequelize');
const db = require('../../config/sequelize');

const Item = db.define('item', {
    name: {
        type: sequelize.STRING,
        unique: {
            msg: "Name already exists"
        },
        allowNull: false,
    },
    whole_price: {
        type: sequelize.DECIMAL(10, 2),
        allowNull: false,
    },
    unit_price: {
        type: sequelize.DECIMAL(10, 2),
        allowNull: false
    },
    image: {
        type: sequelize.STRING,
        allowNull: true,
    }
}, {
    underscored: true
});

Item.sync();

module.exports = Item;
