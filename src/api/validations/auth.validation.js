const Joi = require('joi');

module.exports = {
    signup: {
        body: {
            email: Joi.string().email().required(),
            password: Joi.string().required().min(6).max(128),
        },
    },

    login: {
        body: {
            email: Joi.string().email().required(),
            password: Joi.string().required().min(6).max(128),
        },
    },
};