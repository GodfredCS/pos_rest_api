const router = require('express').Router();
const multer = require('multer');
const footballController = require('../controllers/football');

router.get('/', footballController.getAll);
router.get('/filter', footballController.getByDate);
router.post('/', footballController.create);
router.get('/:id', footballController.get);
router.put('/:id', footballController.update);
router.delete('/:id', footballController.delete);

module.exports = router;