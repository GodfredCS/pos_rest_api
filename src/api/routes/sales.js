const router = require('express').Router();
const multer = require('multer');
const salesController = require('../controllers/sales');

router.get('/', salesController.getAll);
router.get('/filter', salesController.getByDate);
router.post('/', salesController.create);
router.get('/:id', salesController.get);
router.put('/:id', salesController.update);
router.delete('/:id', salesController.delete);

module.exports = router;