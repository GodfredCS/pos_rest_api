const router = require('express').Router();
const upload = require('../services/upload');
const itemsController = require('../controllers/items');

router.get('/', itemsController.getAll);
router.post('/', upload.single('image'), itemsController.create);
router.get('/:id', itemsController.get);
router.put('/:id', upload.single('image'), itemsController.update);
router.delete('/:id', itemsController.delete);

module.exports = router;