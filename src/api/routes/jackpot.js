const router = require('express').Router();
const multer = require('multer');
const jackpotController = require('../controllers/jackpot');

router.get('/', jackpotController.getAll);
router.get('/filter', jackpotController.getByDate);
router.post('/', jackpotController.create);
router.get('/:id', jackpotController.get);
router.put('/:id', jackpotController.update);
router.delete('/:id', jackpotController.delete);

module.exports = router;