const router = require('express').Router();
const creditTransfersController = require('../controllers/credit_transfers');

router.get('/', creditTransfersController.getAll);
router.get('/filter', creditTransfersController.getByDate);
router.post('/', creditTransfersController.create);
router.get('/:id', creditTransfersController.get);
router.put('/:id', creditTransfersController.update);
router.delete('/:id', creditTransfersController.delete);

module.exports = router;