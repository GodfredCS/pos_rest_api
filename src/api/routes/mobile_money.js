const router = require('express').Router();
const multer = require('multer');
const mobileMoneyController = require('../controllers/mobile_money');

router.get('/', mobileMoneyController.getAll);
router.get('/filter', mobileMoneyController.getByDate);
router.post('/', mobileMoneyController.create);
router.get('/:id', mobileMoneyController.get);
router.put('/:id', mobileMoneyController.update);
router.delete('/:id', mobileMoneyController.delete);

module.exports = router;