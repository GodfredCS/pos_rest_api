require('dotenv').config();

module.exports = {
    port: process.env.PORT,
    db_connection: process.env.DB_CONNECTION,
    db_host: process.env.DB_HOST,
    db_database: process.env.DB_DATABASE,
    db_username: process.env.DB_USERNAME,
    db_password: process.env.DB_PASSWORD,
    jwt_key: process.env.JWT_KEY,
    admin_email: process.env.ADMIN_EMAIL,
    admin_password: process.env.ADMIN_PASSWORD,
};


