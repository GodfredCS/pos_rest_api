const Sequelize = require('sequelize');
const { 
    db_database, 
    db_username,
    db_password, 
    db_host, 
    db_connection, 
} = require('./variables');

const db = new Sequelize(db_database, db_username, db_password, {
    host: db_host,
    dialect: db_connection,
}, {
    underscored: true
});

module.exports = db;

// Import and Sync Models.
const Role = require('../api/models/Role');
const User = require('../api/models/User');
const Item = require('../api/models/Item');
const Sale = require('../api/models/Sale');
const MobileMoney = require('../api/models/Mobile_money');
const CreditTransfer = require('../api/models/Credit_transfer');
const Football = require('../api/models/Football');
const Jackpot = require('../api/models/Jackpot');

Role.hasMany(User);
User.belongsTo(Role);
Item.hasMany(Sale);
Sale.belongsTo(Item);