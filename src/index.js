const { port } = require('./config/variables');
const app = require('./config/express');

app.listen(port);